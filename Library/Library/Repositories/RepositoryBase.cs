﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Library.Repositories
{
    public abstract class RepositoryBase<C, T> :
    IRepository<T> where T : class where C : DbContext, new()
    {
        protected RepositoryBase(C context)
        {
            Context = context;
        }

        public C Context { get; set; }

        public virtual IQueryable<T> GetAll()
        {
            IQueryable<T> query = Context.Set<T>();
            return AddIncludes(query);
        }

        public virtual IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = Context.Set<T>().Where(predicate);
            return AddIncludes(query);
        }

        public virtual void Add(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public virtual void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            Context.SaveChanges();
        }

        public virtual IQueryable<T> AddIncludes(IQueryable<T> query)
        {
            return query;
        }
    }
}