﻿using System;
using Library.Models;

namespace Library.Repositories
{
    public interface IBookRepository : IRepository<Book>
    {
        Book GetById(Guid bookId);
    }
}