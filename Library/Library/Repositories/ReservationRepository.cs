﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Models;

namespace Library.Repositories
{
    public class ReservationRepository : RepositoryBase<LibraryEntities, Reservation>, IReservationRepository
    {
        public ReservationRepository(LibraryEntities context)
            : base(context)
        {
        }
    }
}