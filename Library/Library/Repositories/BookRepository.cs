﻿using System;
using System.Data.Entity;
using System.Linq;
using Library.Models;

namespace Library.Repositories
{
    public class BookRepository : RepositoryBase<LibraryEntities, Book>, IBookRepository
    {
        public BookRepository(LibraryEntities context)
            : base(context)
        {
        }
        
        public Book GetById(Guid bookId)
        {
            var query = AddIncludes(Context.Books);
            return query.FirstOrDefault(x => x.Id == bookId);
        }

        public override IQueryable<Book> AddIncludes(IQueryable<Book> query)
        {
            return query.Include(x => x.Reservations);
        }
    }
}