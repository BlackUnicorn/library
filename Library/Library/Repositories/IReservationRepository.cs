﻿using Library.Models;

namespace Library.Repositories
{
    public interface IReservationRepository : IRepository<Reservation>
    {
    }
}