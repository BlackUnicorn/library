﻿namespace Library.Infrastructure
{
    public interface IAuthProvider
    {
        bool Authenticate(string username, string password);
        void SignOut();
    }
}