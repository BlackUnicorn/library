﻿using System;
using System.Web.Mvc;
using Library.Models;
using Library.Repositories;

namespace Library.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IReservationRepository _repository;

        public ReservationController(IReservationRepository repo)
        {
            _repository = repo;
        }

        public JsonResult Reserve(Guid id)
        {
            var from = DateTime.Now.Date;
            var to = from.AddMonths(1);
            var reservation = new Reservation { BookId = id, From = from, To = to, User = this.User.Identity.Name };
            _repository.Add(reservation);
            _repository.Save();
            return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
        }
    }
}