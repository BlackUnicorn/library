﻿using System;
using System.Linq;
using System.Web.Mvc;
using Library.Mappers;
using Library.Models;
using Library.Repositories;

namespace Library.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
        private readonly IBookRepository _repository;

        public BookController(IBookRepository repo)
        {
            _repository = repo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllProducts()
        {
            return Json(BookMapper.Map(_repository.GetAll()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllReservedBooks()
        {
            return Json(BookMapper.Map(_repository.FindBy(x => x.Reservations.Any(y => y.Return == null && y.User == this.User.Identity.Name))), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(Book item)
        {
            _repository.Add(item);
            _repository.Save();
            return Json(item, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Edit(Book product)
        {
            var oldBook = _repository.GetById(product.Id);
            _repository.Edit(oldBook);
            oldBook.Name = product.Name;
            oldBook.Release = product.Release;
            oldBook.Author = product.Author;
            oldBook.Description = product.Description;
            _repository.Save();
            return Json(BookMapper.Map(oldBook), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(Guid id)
        {
            var oldBook = _repository.GetById(id);
            _repository.Delete(oldBook);
            _repository.Save();
            return Json(new { Status = true }, JsonRequestBehavior.AllowGet);
        }

    }
}