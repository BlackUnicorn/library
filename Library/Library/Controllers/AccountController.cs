﻿using System.Web.Mvc;
using Library.Infrastructure;
using Library.Models.ViewModels;

namespace Library.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthProvider authProvider;

        public AccountController(IAuthProvider authProvider)
        {
            this.authProvider = authProvider;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(loginViewModel.UserName, loginViewModel.Password))
                {
                    return Redirect(returnUrl ?? Url.Action("Index", "Book"));
                }
                else
                {
                    ModelState.AddModelError("", "Nieprawidłowa nazwa użytkownika lub hasło.");
                    return View();
                }
            }
            else
            {
                return View();
            }
        }

        [Authorize]
        public JsonResult GetAccount()
        {
            return Json(this.User.Identity.Name, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Logout()
        {
            authProvider.SignOut();
            return RedirectToAction("Index", "Book");
        }
    }
}