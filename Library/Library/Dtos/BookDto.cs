﻿using System;
using System.Collections.Generic;

namespace Library.Dtos
{
    public class BookDto
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public DateTime Release { get; set; }
        public bool IsBooked { get; set; }
        public IList<ReservationDto> Reservations { get; set; }
    }
}