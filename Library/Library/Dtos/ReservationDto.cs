﻿using System;

namespace Library.Dtos
{
    public class ReservationDto
    {
        public Guid Id { get; set; }
        public Guid BookId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public DateTime? Return { get; set; }
        public string User { get; set; }
    }
}