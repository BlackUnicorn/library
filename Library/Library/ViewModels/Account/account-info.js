﻿function AccountInfoViewModel(params) {
    function Book(book, userName) {
        let self = this;
        self.Name = book.Name;
        self.Id = book.Id;
        let reservation = book.Reservations.filter(function (element) { return element.User === userName && element.Return === null; })[0];
        self.From = new Date(parseInt(reservation.From.substr(6)));
        self.To = new Date(parseInt(reservation.To.substr(6)));
    }
    let self = this;
    self.UserName = ko.observable("");
    self.Books = ko.observableArray();
    $.ajax({
        url: '/Account/GetAccount',
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        success: function (data) {
            self.UserName(data);
            $.ajax({
                url: '/Book/GetAllReservedBooks',
                cache: false,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                data: {},
                success: function (data) {
                    for (book of data) {
                        self.Books.push(new Book(book, self.UserName()));
                    }
                }
            });
        }
    });
}

ko.components.register('account-info', {
    viewModel: AccountInfoViewModel,
    template: { element: 'account-info-template' }
        //"<div>< table data-bind=\"visible: Books().length > 0\" class= \"table table-striped\" ><thead><tr><th>Name</th><th>From</th><th>To</th></tr></thead><tbody data-bind=\"foreach: Books()\"><tr><td data-bind=\"text: Name\"></td><td data-bind=\"text: From\"></td><td data-bind=\"text: To\"></td></tr></tbody></table></div>"
});