﻿function Book(book) {
    let self = this;
    self.Name = ko.observable(book === null ? "" : book.Name);
    self.Author = ko.observable(book === null ? "" : book.Author);
    self.Description = ko.observable(book === null ? "" : book.Description);
    let date = book === null ? "" : typeof book.Release === "string" ? new Date(parseInt(book.Release.substr(6))) : book.Release;
    self.Release = ko.observable(date);
    self.ReleaseString = ko.computed(function () { return ReleaseString(self.Release()); });
    self.Id = book === null ? null : book.Id;
    self.IsBooked = ko.observable(book === null ? false : book.IsBooked);
}

function ReleaseString(date) {
    if (date === null || date === "") {
        return null;
    }
    else {
        let parsedDate = new Date(date);
        return parsedDate.getMonth() + 1 + "." + parsedDate.getFullYear();
    }
}

function BookIndexViewModel() {
    let self = this;
    self.SelectedBook = ko.observable(new Book(null));
    self.Books = ko.observableArray();
    self.IsAdmin = ko.observable("");
    $.ajax({
        url: '/Book/GetAllProducts',
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        success: function (data) {
            for (book of data) {
                self.Books.push(new Book(book));
            }
        }
    });

    $.ajax({
        url: '/Account/GetAccount',
        cache: false,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        data: {},
        success: function (data) {
            self.IsAdmin(data === "admin");
        }
    });
    

    self.create = function () {
        let book = self.SelectedBook();
        if (book.Name() !== "" &&
            book.Author() !== "" &&
            book.Description() !== "" &&
            book.Release() !== "") {
            $.ajax({
                url: '/Book/Add',
                cache: false,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: ko.toJSON(book),
                success: function (data) {
                    self.Books.push(new Book(data));
                    self.SelectedBook(new Book(null));
                }
            }).fail(
                function (xhr, textStatus, err) {
                    alert(err);
                });
        }
        else {
            alert('Please Enter All the Values !!');
        }
    };
    self.delete = function (book) {
        if (confirm('Are you sure to Delete "' + book.Name() + '"?')) {
            var Id = book.Id;

            $.ajax({
                url: '/Book/Delete/' + Id,
                cache: false,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                data: {},
                success: function (data) {
                    self.Books.remove(book);
                }
            }).fail(
                function (xhr, textStatus, err) {
                    self.status(err);
                });
        }
    };

    self.reserve = function (book) {
        if (confirm('Are you sure to reserve "' + book.Name() + '"?')) {
            var Id = book.Id;

            $.ajax({
                url: '/Reservation/Reserve/' + Id,
                cache: false,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                data: {},
                success: function (data) {
                    book.IsBooked(true);
                }
            }).fail(
                function (xhr, textStatus, err) {
                    self.status(err);
                });
        }
    };

    self.edit = function (book) {
        self.SelectedBook(new Book({ Name: book.Name(), Author: book.Author(), Description: book.Description(), Release: book.Release(), Id: book.Id, IsBooked: book.IsBooked}));
    };
    
    self.update = function () {
        var product = self.SelectedBook();

        $.ajax({
            url: '/Book/Edit',
            cache: false,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: ko.toJSON(product),
            success: function (data) {
                self.Books.remove(function (book) { return book.Id === product.Id; });
                self.Books.push(new Book(data));
                self.SelectedBook(new Book(null));
                alert("Record Updated Successfully");
            }
        }).fail(
                function (xhr, textStatus, err) {
                    alert(err);
                });
    };

    // Cancel product details
    self.cancel = function () {
        self.SelectedBook(new Book(null));
    };
}
ko.applyBindings(new BookIndexViewModel());