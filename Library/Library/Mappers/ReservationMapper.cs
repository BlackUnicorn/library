﻿using System.Collections.Generic;
using Library.Dtos;
using Library.Models;

namespace Library.Mappers
{
    public class ReservationMapper
    {
        public static ReservationDto Map(Reservation entity)
        {
            return new ReservationDto
            {
                Id = entity.Id,
                BookId = entity.BookId,
                From = entity.From,
                To = entity.To,
                Return = entity.Return,
                User = entity.User,
            };
        }

        public static IEnumerable<ReservationDto> Map(IEnumerable<Reservation> entities)
        {
            foreach (var e in entities)
            {
                yield return Map(e);
            }
        }
    }
}