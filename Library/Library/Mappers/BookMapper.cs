﻿using System.Collections.Generic;
using System.Linq;
using Library.Dtos;
using Library.Models;

namespace Library.Mappers
{
    public static class BookMapper
    {
        public static BookDto Map(Book entity)
        {
            return new BookDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Author = entity.Author,
                Description = entity.Description,
                Release = entity.Release,
                IsBooked = entity.Reservations != null && entity.Reservations.Any(x => x.Return == null),
                Reservations = ReservationMapper.Map(entity.Reservations).ToList(),
            };
        }

        public static IEnumerable<BookDto> Map(IEnumerable<Book> entities)
        {
            foreach (var book in entities)
            {
                yield return Map(book);
            }
        }
    }
}